import { Collection } from 'discord.js';
import fs from 'fs';
import Command from '@interfaces/Command';
import Module from '@interfaces/Module';

export default class Loader {

  private commands = new Collection<string, Command>()
  private modules = new Collection<string, Module>()
  private activeListeners = new Collection<string, Function>()

  get getCommands(): Collection<string, Command> { return this.commands; }
  get getModules(): Collection<string, Module> { return this.modules; }
  get getActiveListeners(): Collection<string, Function> { return this.activeListeners; }

  set setCommands(value: Collection<string, Command>) { this.commands = value; }
  set setModules(value: Collection<string, Module>) { this.modules = value; }
  set setActiveListeners(value: Collection<string, Function>) { this.activeListeners = value; }

  load(path: string) {
    const modulesFolder = fs.readdirSync(path);
    modulesFolder.forEach((_module) => {
      delete require.cache[`${path}/${_module}`];

      const nayuModule = require(`${path}/${_module}`);
      this.modules.set(nayuModule.name, nayuModule);

      nayuModule.commands.forEach((command: Command) => {
        this.commands.set(command.name, command);
      });
    });
  }
}

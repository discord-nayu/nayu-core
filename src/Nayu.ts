import { Client, Collection } from 'discord.js';
import Command from '@interfaces/Command';
import Module from '@interfaces/Module';
import Loader from './Loader';

export default class Nayu extends Client {

  private modules: Collection<string, Module>
  private commands: Collection<string, Command>
  private loader: Loader

  constructor() {
    super();
    this.loader = new Loader();
    this.loader.load(`${__dirname}/modules`);
    this.modules = this.loader.getModules;
    this.commands = this.loader.getCommands;
  }

  get getCommands(): Collection<string, Command> { return this.commands; }
  get getModules(): Collection<string, Module> { return this.modules; }
  get getLoader(): Loader { return this.loader; }

  reload() {
    this.loader.load(`${__dirname}/modules`);
    this.modules = this.loader.getModules;
    this.commands = this.loader.getCommands;
  }
}

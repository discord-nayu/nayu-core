import fs from 'fs';
import { Message, MessageEmbed } from 'discord.js';
import Nayu from './Nayu';

require('dotenv').config(); // require needed here

const Nayubot = new Nayu();

const init = () => {
  if (!fs.existsSync(`${__dirname}/data`)) {
    const LoaderSnapshot = Nayubot.getLoader;
    fs.mkdirSync(`${__dirname}/data`);
    fs.writeFileSync(`${__dirname}/data/loader.json`, JSON.stringify(LoaderSnapshot));
  }
};

const createErrorEmbed = (message: string, fix: string) => {
  const err = new MessageEmbed();
  err.setTitle(message);
  err.addField('fix:', fix);
  err.setFooter('try again!');
  return err;
};

const parseMessage = (message: Message) => {
  const command = message.content.split(' ')[1];
  const args = message.content.split(' ').slice(2);

  if (message.content.startsWith('nayu')) {
    Nayubot.reload();
    const nayuCommands = Nayubot.getLoader.getCommands;
    const toBeExecuted = nayuCommands.get(command) || nayuCommands.find(
      (cmd) => cmd.aliases && cmd.aliases.includes(command),
    );
    if (toBeExecuted) {
      toBeExecuted.execute(Nayubot, message, args);
    } else {
      message.channel.send(createErrorEmbed(`Unknown command ${command}`, 'the command you used is unavailable.'));
    }
  }
};
Nayubot.on('ready', () => {
  // eslint-disable-next-line no-console
  console.log(`Hi! Nayubot instance running as ${Nayubot.user.tag}`);
});
Nayubot.on('message', (message: Message) => {
  if (message.author.bot) {
    return;
  }
  parseMessage(message);
});

Nayubot.login(process.env.BOT_TOKEN).then((val) => {
  init();
});
